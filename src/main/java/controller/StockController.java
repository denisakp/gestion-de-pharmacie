/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static connexion.ConnexionDB.ConnexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Stock;

/**
 *
 * @author eunix
 */
public class StockController {
    
        Connection connection; 
        PreparedStatement preparedStatement;
        Statement statement;
        Stock stock;

    public StockController() {
        connection = (Connection) ConnexionDB();
    }
    
    
    public void insert(Stock stock){
        String query = "INSERT INTO stock (id_med,qte_medc,qte_avant) VALUES (?,?,?)";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, stock.getIdMed());
            preparedStatement.setLong(2, stock.getQteMedc());
            preparedStatement.setLong(3, stock.getQteAvant());
            preparedStatement.executeUpdate();
            
            preparedStatement.close();
            connection.close();
 
        } catch (SQLException ex) {
            Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
    }

    public void delete(long id){
        String query = "DELETE FROM stock WHERE id_med=?";
         try {
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();

             preparedStatement.close();
             connection.close();
         } catch (SQLException ex) {
             Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
    }
    
    public void update(Stock stock){

        String query ="UPDATE stock SET qte_medc=qte_medc+'"+stock.getQteMedc()+"' WHERE id_med=?";
        
         try { 
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, stock.getIdMed());
             preparedStatement.executeUpdate();
             
             preparedStatement.close();
             connection.close();

         } catch (SQLException ex) {
             Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
    }
    
    public void updateFirstStock(Stock stock){
        String query = "UPDATE stock  SET qte_avant=qte_avant+'"+stock.getQteAvant()+"' where id_med=?";
        
         try { 
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, stock.getIdMed());
             
             preparedStatement.executeUpdate();
             
             connection.close();
             
         } catch (SQLException ex) {
             Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex.getMessage()); 
         }
    }
    
    public void updateVenteStock(Stock stock){
        String query = "UPDATE stock set qte_avant=qte_avant-'"+stock.getQteAvant()+"' where id_med=?";
        
         try { 
             preparedStatement = connection.prepareStatement(query);            
             preparedStatement.setLong(1, stock.getIdMed());           
             preparedStatement.executeUpdate(); 
             
             connection.close();
             
         } catch (SQLException ex) {
             Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
    }
    
    public void updateRavStock(Stock stock){
        String query = "UPDATE  stock SET qte_medc=qte_medc-'"+stock.getQteMedc()+"' where id_med=?";
        
         try { 
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, stock.getIdMed());
             preparedStatement.executeUpdate();
             
             preparedStatement.close();
             connection.close();
             
         } catch (SQLException ex) {
             Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
    }
    
    public List<Stock> index(){
        List<Stock> stockList= new ArrayList<>();
        String query = "SELECT * FROM Stock"; 
        
         try { 
             statement = connection.createStatement();
             ResultSet rest = statement.executeQuery(query);
              while (rest.next()){
                  stock=new Stock(rest.getLong(1),rest.getLong(2),rest.getLong(3),rest.getLong(4));
                  stockList.add(stock);             
              }
              
              connection.close();
              
         } catch (SQLException ex) {
             Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
         return stockList;
    }
    
    public int getFirstStock(Long id){
        int nbre = 0;
         String query = "SELECT qte_avant FROM stock WHERE id_med='"+id+"'"; 
         try { 
             statement = connection.createStatement();
             ResultSet rest = statement.executeQuery(query);
              while (rest.next()){
                  nbre = rest.getInt(1);            
              }
              connection.close();
              
         } catch (SQLException ex) {
             Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
         return nbre;
    }

    public Stock showOne(Long id){
         String query = "SELECT * FROM stock WHERE id_med='"+id+"'"; 
         try { 
             
             statement = connection.createStatement();
             ResultSet rest = statement.executeQuery(query);
             
              while (rest.next()){
                stock = new Stock(rest.getLong(1),rest.getLong(2),rest.getLong(3),rest.getLong(4));            
              }
              
              connection.close();
              
         } catch (SQLException ex) {
             Logger.getLogger(StockController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
         return stock;
    }
    
}
