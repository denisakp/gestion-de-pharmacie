/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static connexion.ConnexionDB.ConnexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Vente;

/**
 *
 * @author eunix
 */
public class VenteController {
    Connection connection;
    PreparedStatement preparedStatement;
    Statement statement;
    
    public VenteController() {
        connection = (Connection) ConnexionDB();  
    }
    
    public void store(Vente vente) {
        String query = "INSERT INTO vente(qte_vdu,date_vente,montant_vente) VALUES (?,?,?)";
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, vente.getQteVdu());
             preparedStatement.setDate(2, vente.getDateVente());
             preparedStatement.setFloat(3, vente.getMontantVente());
                preparedStatement.executeUpdate();
                
                preparedStatement.close();
                connection.close();
        } catch (SQLException e) {
            Logger.getLogger(VenteController.class.getName()).log(Level.SEVERE, null, e.getMessage());
        }
    }
    
    public Long getLastVente(){ 
        Long id =0L;
        String query = "SELECT max(id_vente) FROM vente"; 
         try { 
             statement = connection.createStatement();            
             ResultSet rest = statement.executeQuery(query);
             while (rest.next()){
                  id = rest.getLong(1);                  
              }
               statement.close();

               connection.close();
         } catch (SQLException ex) {
             Logger.getLogger(VenteController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
         return id;
    }
    
    public List<Vente> index(){     
        List<Vente> venteList= new ArrayList<>();
        String query = "SELECT v.id_vente, v.qte_vdu, v.montant_vente, v.date_vente, m.designation from vente v, medicament m, medicamentvente mv where v.id_vente=mv.ID_VENTE and m.id_med=mv.ID_MED "; 
         try { 
             statement = connection.createStatement();            
             ResultSet rest = statement.executeQuery(query);
             while (rest.next()){
                  Vente vente = new Vente(rest.getLong(1),rest.getLong(2),rest.getString(4),rest.getFloat(3),rest.getString(5));
                  venteList.add(vente);
              }
             statement.close();
             connection.close();
                         
         } catch (SQLException ex) {
             Logger.getLogger(VenteController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
         return venteList;
    }

    public void update(Vente vente){
        String query = "UPDATE Vente set qte_vdu=?,montant_vente=? WHERE id_vente=?";
        
         try { 
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, vente.getQteVdu());
             preparedStatement.setFloat(2, vente.getMontantVente());
             preparedStatement.setLong(3,vente.getIdVente());
             preparedStatement.executeUpdate();
             
             preparedStatement.close();
             connection.close();

         } catch (SQLException ex) {
             Logger.getLogger(VenteController.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
    public void delete(long id){
        String query = "DELETE FROM vente WHERE id_vente=?";
         try {
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, id);
             preparedStatement.executeUpdate();
             
             preparedStatement.close();
             connection.close();

         } catch (SQLException ex) {
             Logger.getLogger(VenteController.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
    }

}
