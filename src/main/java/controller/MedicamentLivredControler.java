/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static connexion.ConnexionDB.ConnexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.MedicamentLivrer;

/**
 *
 * @author eunix
 */
public class MedicamentLivredControler {
    Connection connection; 
    PreparedStatement preparedStatement;
    MedicamentLivrer livrer;

    public MedicamentLivredControler() {
        connection = (Connection) ConnexionDB();
    }
     
    public void insert(MedicamentLivrer medicamentliv){   
        String query = "INSERT INTO medicamentliv(id_med,id_liv) VALUES(?,?)";
        try { 
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setLong(1, medicamentliv.getIdMed());
             preparedStatement.setLong(2, medicamentliv.getIdLiv());
             
            preparedStatement.executeUpdate();
            
            preparedStatement.close();
            connection.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(MedicamentLivredControler.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
    }
 
    public void update(MedicamentLivrer medicamentliv){
        String query = "update medicamentliv set ID_MED=? where ID_LIV=?";
        
         try { 
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, medicamentliv.getIdMed());
             preparedStatement.setLong(2, medicamentliv.getIdLiv());
             
             preparedStatement.executeUpdate();
             
             preparedStatement.close();
             connection.close();
             
         } catch (SQLException ex) {
             Logger.getLogger(MedicamentLivredControler.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
    }
    
    public void delete(long id){
        String query="Delete from Medicamentliv where id_liv=?";
        
         try {
             preparedStatement = connection.prepareStatement(query);
             preparedStatement.setLong(1, id);
             
             preparedStatement.executeUpdate();
               preparedStatement.close();
               connection.close();
         } catch (SQLException ex) {
             Logger.getLogger(MedicamentLivredControler.class.getName()).log(Level.SEVERE, null, ex.getMessage());
         }
    }
    
}
