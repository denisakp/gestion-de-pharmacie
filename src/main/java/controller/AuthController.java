/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import connexion.ConnexionDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Pharmacien;

/**
 *
 * @author eunix
 */
public class AuthController {
    ConnexionDB connexion;
    Statement statement;
    Connection connection;
    
    public Pharmacien login (String userName, String passwd ) {
        Pharmacien user = new Pharmacien();
        String query = "SELECT id, nom, prenom,fonction FROM pharmacien WHERE nom= '"+userName+"' and password= '"+passwd+"'";
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if(resultSet.next()){
                user = new Pharmacien(resultSet.getLong(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4));
            }
        } catch (SQLException e) {
            Logger.getLogger(AuthController.class.getName()).log(Level.SEVERE, null, e);
            System.out.println(e.getMessage());
        }
        return user;
    }
}
