/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author eunix
 */
public class Stock {
    private long numMedc; 
   private long idMed; 
   private long qteMedc; 
   private long qteAvant; 

    public Stock() {
    }

    public Stock(long numMedc, long idMed, long qteMedc, long qteAvant) {
        this.numMedc = numMedc;
        this.idMed = idMed;
        this.qteMedc = qteMedc;
        this.qteAvant = qteAvant;
    }

    public Stock(long idMed, long qteMedc, long qteAvant) {
        this.idMed = idMed;
        this.qteMedc = qteMedc;
        this.qteAvant = qteAvant;
    }

    public Stock(long idMed, long qteAvant) {
        this.idMed = idMed;
        this.qteAvant = qteAvant;
    }
    
    

    public long getNumMedc() {
        return numMedc;
    }

    public void setNumMedc(long numMedc) {
        this.numMedc = numMedc;
    }

    public long getIdMed() {
        return idMed;
    }

    public void setIdMed(long idMed) {
        this.idMed = idMed;
    }

    public long getQteMedc() {
        return qteMedc;
    }

    public void setQteMedc(long qteMedc) {
        this.qteMedc = qteMedc;
    }

    public long getQteAvant() {
        return qteAvant;
    }

    public void setQteAvant(long qteAvant) {
        this.qteAvant = qteAvant;
    }
    @Override
     public String toString() {
        return "Stock{" + "numMedc=" + numMedc + ", idMed=" + idMed + ", qteMedc=" + qteMedc + ", qteAvant=" + qteAvant + '}';
    }
    
}
