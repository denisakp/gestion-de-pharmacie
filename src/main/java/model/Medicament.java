/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author eunix
 */
public class Medicament {
    private long idMed; 
    private String designation; 
    private String type; 
    private float prix;

    public Medicament() {
    }

    public Medicament(long idMed, String designation, String type, float prix) {
        this.idMed = idMed;
        this.designation = designation;
        this.type = type;
        this.prix = prix;
    }

    public Medicament(String designation, String type, float prix) {
        this.designation = designation;
        this.type = type;
        this.prix = prix;
    }



    public Medicament(String designation, String type) {
        this.designation = designation;
        this.type = type;
    }

    public Medicament(String designation) {
        this.designation = designation;
    }
    
    

    public long getIdMed() {
        return idMed;
    }

    public void setIdMed(long idMed) {
        this.idMed = idMed;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }
    @Override
     public String toString() {
        return "Medicament{" + "idMed=" + idMed + ", designation=" + designation + ", type=" + type + ", prix=" + prix + '}';
    }
    
}
