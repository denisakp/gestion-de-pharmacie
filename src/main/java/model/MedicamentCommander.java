/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author eunix
 */
public class MedicamentCommander {
    private long idMed; 
    private long idCmde; 

    public MedicamentCommander() {
    }

    public MedicamentCommander(long idMed, long idCmde) {
        this.idMed = idMed;
        this.idCmde = idCmde;
    }

    public long getIdMed() {
        return idMed;
    }

    public void setIdMed(long idMed) {
        this.idMed = idMed;
    }

    public long getIdCmde() {
        return idCmde;
    }

    public void setIdCmde(long idCmde) {
        this.idCmde = idCmde;
    }
    @Override
     public String toString() {
        return "MedicamentCom{" + "idMed=" + idMed + ", idCmde=" + idCmde + '}';
    }
    
}
