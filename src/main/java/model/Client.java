/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author eunix
 */
public class Client {
    private long idClient;
    private String nomClient;
    private String numCarteSS;
    private String telephone; 
    private String adresse;
    
    public Client() {
    }
    
       public Client(long idClient, String nomClient, String numCarteSS, String telephone, String adresse) {
        this.idClient = idClient;
        this.nomClient = nomClient;
        this.numCarteSS = numCarteSS;
        this.telephone = telephone;
        this.adresse = adresse;
    }
       
     public Client(String nomClient, String numCarteSS, String telephone, String adresse) {
        this.nomClient = nomClient;
        this.numCarteSS = numCarteSS;
        this.telephone = telephone;
        this.adresse = adresse;
    }
     
    public long getIdClient() {
        return idClient;
    }

    public void setIdClient(long idClient) {
        this.idClient = idClient;
    }
    
     public String getNomClient() {
        return nomClient;
    }

    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }
    
    public String getNumCarteSS() {
        return numCarteSS;
    }

    public void setNumCarteSS(String numCarteSS) {
        this.numCarteSS = numCarteSS;
    }
    
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    
    
    @Override
     public String toString() {
        return "Client{" + "idClient=" + idClient + ", nomClient=" + nomClient + ", numCarteSS=" + numCarteSS + ", telephone=" + telephone + ", adresse=" + adresse + '}';
    }
    


    
}
