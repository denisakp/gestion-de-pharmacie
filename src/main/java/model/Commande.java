/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author eunix
 */
public class Commande {
    private long idCmde; 
    private String designation;
    private long idPhar;
    private String numCmde; 
    private long qteCmde; 
    private Date dateCmde;
    private Date dateSaisie;
    
    public Commande() {
        
    }
     
    public Commande(long idCmde, String designation, long idPhar, String numCmde, long qteCmde, Date dateCmde) {
        this.idCmde = idCmde;
        this.designation = designation;
        this.idPhar = idPhar;
        this.numCmde = numCmde;
        this.qteCmde = qteCmde;
        this.dateCmde = dateCmde;
    }
    
    public Commande(long idCmde, long idPhar, String numCmde, long qteCmde, Date dateCmde) {
        this.idCmde = idCmde;
        this.idPhar = idPhar;
        this.numCmde = numCmde;
        this.qteCmde = qteCmde;
        this.dateCmde = dateCmde;
    }
    
    public Commande(long idPhar, String numCmde, long qteCmde, Date dateCmde) {
        this.idPhar = idPhar;
        this.numCmde = numCmde;
        this.qteCmde = qteCmde;
        this.dateCmde = dateCmde;
    }
    
    public long getIdCmde() {
        return idCmde;
    }
    
    public void setIdCmde(long idCmde) {
        this.idCmde = idCmde;
    }

    public long getIdPhar() {
        return idPhar;
    }
    public void setIdPhar(long idPhar) {
        this.idPhar = idPhar;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }
    public Date getDateCmde() {
        return dateCmde;
    }

    public void setDateCmde(Date dateCmde) {
        this.dateCmde = dateCmde;
    }
    
    public String getNumCmde() {
        return numCmde;
    }

    public void setNumCmde(String numCmde) {
        this.numCmde = numCmde;
    }
    public long getQteCmde() {
        return qteCmde;
    }

    public void setQteCmde(long qteCmde) {
        this.qteCmde = qteCmde;
    }

    public Date getDateSaisie() {
        return dateSaisie;
    }
    public void setDateSaisie(Date dateSaisie) {
        this.dateSaisie = dateSaisie;
    }
    
    @Override
    public String toString() {
        return "Commande{" + "idCmde=" + idCmde + ", designation=" + designation + ", idPhar=" + idPhar + ", numCmde=" + numCmde + ", qteCmde=" + qteCmde + ", dateCmde=" + dateCmde + ", dateSaisie=" + dateSaisie + '}';
    }



    
}
