/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author eunix
 */
public class MedicamentLivrer {
    private long idMed; 
    private long idLiv; 

    public MedicamentLivrer() {
    }

    public MedicamentLivrer(long idMed, long idLiv) {
        this.idMed = idMed;
        this.idLiv = idLiv;
    }

    public long getIdMed() {
        return idMed;
    }

    public void setIdMed(long idMed) {
        this.idMed = idMed;
    }

    public long getIdLiv() {
        return idLiv;
    }

    public void setIdLiv(long idLiv) {
        this.idLiv = idLiv;
    }
    @Override
     public String toString() {
        return "MedicamentLiv{" + "idMed=" + idMed + ", idLiv=" + idLiv + '}';
    }
    
}
